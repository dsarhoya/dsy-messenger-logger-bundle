<?php

namespace DSY\DSYMessengerLoggerBundle;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use DSY\DSYMessengerLoggerBundle\Entity\LoggerMessage;
use DSY\DSYMessengerLoggerBundle\Repository\LoggerMessageRepository;

class DSYMessengerLogger
{
    private $entityManager;

    //TODO: agregar tipo de mensajes para obtener diferentes logs.
    // default, warning, error, constantes de la entidad (?) =.

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function log($message = null, $context = null, $type = null)
    {
        if (null == $message) {
            return 0;
        }
        if (null === $type) {
            $type = 'default';
        }
        $loggerInfo = new LoggerMessage();
        $loggerInfo->setDateTime(new DateTime('now'));
        $loggerInfo->setMessage($message);
        $loggerInfo->setContext($context);
        $loggerInfo->setType($type);

        $this->entityManager->persist($loggerInfo);
        $this->entityManager->flush();
    }

    public function getLogs(array $options)
    {
        /** @var LoggerMessageRepository $repoLogger */
        $repoLogger = $this->entityManager->getRepository(LoggerMessage::class);

        $logs = $repoLogger->logger($options);

        return $logs;
    }
}
