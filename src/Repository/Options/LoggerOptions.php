<?php

namespace DSY\DSYMessengerLoggerBundle\Repository\Options;

use Symfony\Component\OptionsResolver\OptionsResolver;

class LoggerOptions extends OptionsResolver
{
    public $options;

    public function __construct(array $options = [])
    {
        $this->configureOptions($this);

        $this->options = $this->resolve($options);
    }

    public function __get($name)
    {
        return $this->options[$name];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'date' => null,
           'context' => null,
           'query_builder' => null,
           'contextsArray' => null,
           'limit' => null,
       ]);

        $resolver
        ->setAllowedTypes('context', ['null', 'string'])
        ->setAllowedTypes('date', ['null', \DateTime::class])
        ->setAllowedTypes('query_builder', ['null', 'bool'])
        ->setAllowedTypes('contextsArray', ['null', 'array'])
        ->setAllowedTypes('limit', ['null', 'int'])
        ;
    }
}
