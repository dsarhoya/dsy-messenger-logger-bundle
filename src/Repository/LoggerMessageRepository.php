<?php

namespace DSY\DSYMessengerLoggerBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DSY\DSYMessengerLoggerBundle\Entity\LoggerMessage;
use DSY\DSYMessengerLoggerBundle\Repository\Options\LoggerOptions;

/**
 * @method LoggerMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method LoggerMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method LoggerMessage[]    findAll()
 * @method LoggerMessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoggerMessageRepository extends EntityRepository
{
    // public function __construct(ManagerRegistry $registry)
    // {
    //     parent::__construct($registry, LoggerMessage::class);
    // }

    public function logger(array $options)
    {
        $options = new LoggerOptions($options);
        $query = $this->createQueryBuilder('log');

        if (null !== $options->limit) {
            $query->setMaxResults($options->limit);
        }

        if (null !== $options->context) {
            $query->andWhere('log.context = :context')
            ->setParameter('context', $options->context);
        }

        if (is_array($options->contextsArray)) {
            $query->andWhere('log.context in (:contexts)')
            ->setParameter('contexts', $options->contextsArray)
            ;
        }

        if (null !== $options->date) {
            $query->andWhere('log.dateTime = :date')
            ->setParameter('date', $options->date);
        }
        if ($options->query_builder) {
            return $query;
        }

        $query->orderBy('log.dateTime', 'DESC');

        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return LoggerMessage[] Returns an array of LoggerMessage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LoggerMessage
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
