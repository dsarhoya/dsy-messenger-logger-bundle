<?php

namespace DSY\DSYMessengerLoggerBundle;

use DSY\DSYMessengerLoggerBundle\DependencyInjection\DSYMessengerLoggerExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DSYMessengerLoggerBundle extends Bundle
{
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new DSYMessengerLoggerExtension();
        }

        return $this->extension;
    }
}
